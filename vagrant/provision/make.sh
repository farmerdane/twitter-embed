# Setup variables
PROJECTNAME='twitter-embed'

export DEBIAN_FRONTEND=noninteractive

# Setup APT
sudo apt-get update --fix-missing
sudo apt-get upgrade

# Setup apache
sudo apt-get install -y -q apache2

if [ -L /etc/apache2/sites-enabled/000-default.conf ]; then
    sudo unlink /etc/apache2/sites-enabled/000-default.conf
fi

read -d '' vhost << EOF
<VirtualHost *:80>
    ServerAdmin ops@example.com

    DocumentRoot /var/www/vhosts/${PROJECTNAME}/src/public
    <Directory />
        Options FollowSymLinks
        AllowOverride None
    </Directory>

    SetEnv APP_ENV vbox

    <Directory /var/www/vhosts/${PROJECTNAME}/>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Order allow,deny
        Allow from all
    </Directory>

    # Possible values include: debug, info, notice, warn, error, crit,
    # alert, emerg.
    LogLevel warn

    ErrorLog /var/www/vhosts/${PROJECTNAME}/src/apache/error.log
    CustomLog /var/www/vhosts/${PROJECTNAME}/src/apache/access.log combined
</VirtualHost>
EOF

sudo echo "$vhost" > /vagrant/provision/default.conf

if [ ! -L /etc/apache2/sites-enabled/default.conf ]; then
    sudo ln -s /vagrant/provision/default.conf /etc/apache2/sites-enabled/default.conf
fi

sudo a2enmod rewrite

sudo update-rc.d -f apache2 disable
sudo service apache2 stop

# Setup PHP
sudo apt-get install -y -q php5 php5-mysql php5-mcrypt php5-curl php5-cli php5-common php5-gd
sudo php5enmod mcrypt

if [ ! -L /etc/php5/apache2/conf.d/10-mcrypt.ini ]; then
    sudo ln -s /vagrant/provision/etc/php5/apache2/conf.d/10-mcrypt.ini /etc/php5/apache2/conf.d/10-mcrypt.ini
fi

if [ ! -L /etc/php5/cli/conf.d/10-mcrypt.ini ]; then
    sudo ln -s /vagrant/provision/etc/php5/cli/conf.d/10-mcrypt.ini /etc/php5/cli/conf.d/10-mcrypt.ini
fi

sudo service apache2 start

# Composer
cd /tmp/
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
cd /var/www/vhosts/$PROJECTNAME/src/public
composer install

# Setup MySQL
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password password'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password password'

sudo apt-get install -q -y mysql-server mysql-client
sudo update-rc.d mysql disable
sudo service mysql restart

# Database setup
#cd /var/www/vhosts/$PROJECTNAME/
#mysqladmin -u root -p'password' create 'bt-sport-business-vbox'
#mysql -u root -p'password' 'bt-sport-business-vbox' < db_migration/schema.sql
#mysql -u root -p'password' 'bt-sport-business-vbox' < db_migration/fixtures.sql
#cd /var/www/vhosts/bt-sport-business/db_migration
#php ../project/vendor/bin/phinx migrate -e bt-sport-business-vbox
