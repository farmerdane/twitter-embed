(function ($) {
	var TC = TC || {};

	TC.TCMain = {
		pusher : null,
		init: function() {
			var m = this;
			$('a[rel^="external"], .external').attr('target', '_blank');

			$('#linkColor').colpick({
				layout:'hex',
				submit:0,
				colorScheme:'light',
				onChange:function(hsb,hex,rgb,el,bySetColor) {
					$(el).css('border-color','#'+hex);
					// Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
					if(!bySetColor) $(el).val(hex);
				}
			}).keyup(function(){
				$(this).colpickSetColor(this.value);
			});

			var lis = $("#container-tweet ul li");
			var ti = $("#container-display");
			var cs = $("#container-select");
			var df = $("#dataform");
			var tid = $("#tid");


			$("#container-tweet").on("click", ".tab-content ul > li", function(el){
				var el = $(this);
				lis.removeClass("selected");
				el.addClass("selected");
				tid.val(el.data("tid"));
				ti.trigger( "tweetrefresh", [  ] );

				var tt = $("#tweet-toggle");
				tt.text("Off");
				m.disconnectPusher();

			});

			cs.find("input").blur(function() {
				ti.trigger( "tweetrefresh", [  ] );
			});

			cs.find("select").change(function(){
				ti.trigger( "tweetrefresh", [  ] );
			});

			df.on("submit", function(e){
				e.preventDefault();
				ti.trigger( "tweetrefresh", [  ] );
			});



			ti.on("tweetrefresh", function(event) {
				$("#tweet-item").text("");
				
				var tweetdata = df.serializeArray();		

				var item = {"id" : null, "params" : {}};

				for(var i in tweetdata){
					var v = tweetdata[i];
					if(v.name == "tid"){
						item.id = v.value;
					}else if(v.name == "linkColor" && v.value != ""){
						item.params[v.name] = "#"+v.value;
					}else{
						if(v.value != ""){
							item.params[v.name] = v.value;
						}
					}
				}

				twttr.widgets.createTweet(
				item.id,
				document.getElementById('tweet-item'),
				item.params)
				.then(function (el) {

				});

				m.fillCodeBox(item);


			});

			$('#myModal').on('show.bs.modal', function (e) {
				ti.trigger( "tweetrefresh", [  ] );
			})


			$("#tweet-toggle").on("click", function(e){
				e.preventDefault();
				var el = $(this);
				m.toggleTweetButton(el);
			});


			$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
				var el = $(e.target);

				if(el.hasClass("live-tweets")){
					if(m.pusher == null){
						m.initPusher();
					}else{
					}
				}
			});



		},

		initPusher : function(){
			this.pusher = new Pusher('4e2db22dedb81cbeb1d1');
			this.channel = this.pusher.subscribe('tweet-channel');
			this.channel.bind('tweet_event', function(data) {

				var tl = $("#tl-live");
				var ul = tl.find("ul");
				ul.fadeOut();
				ul.remove();
				var html = '<ul>';

				for(var i in data){
					var el = data[i][0];
					html += '<li data-tid="'+el.id+'"><div class="media"><a class="media-left" href="#"><img src="'+el.profile_image_url_https+'" alt="..."></a><div class="media-body"><h4 class="media-heading">'+el.name+' (@'+el.screen_name+')</h4><p>Tweet ID: '+el.id+'</p><span>'+el.text+'</span></div></div></li>';
				}
				html += '</ul>';
				tl.append(html);
				tl.find("ul").fadeIn(5000);

			});
		},

		disconnectPusher : function(){
			if(this.pusher){
				this.pusher.disconnect();
				this.pusher = null;
			}
		},
		toggleTweetButton : function(el){
			var m = this;
			if(el.text() == "On"){
				el.text("Off");
				m.disconnectPusher();
			}else{
				el.text("On");
				m.initPusher();
			}
		},

		fillCodeBox :function(item){
			var cc = $("#code-chunk");
			var code = cc.text();
			delete item.params.targetEl;
			delete item.params.tweetId;
			var finalcode = code.replace('{options}', JSON.stringify(item.params)).replace("{id}", item.id);
			$("#code-chunk-container").val(finalcode);
		}
	};


	$(document).ready(function () {
		TC.TCMain.init();
	});

	twttr.ready(function() {

		twttr.widgets.createTweet(
		'495719809695621121',
		document.getElementById('tweet-item-main'),
		{
			align: 'center',
			conversation : "all"
		})
		.then(function (el) {

		});
	});

})(jQuery);

