<?php

global $menu;
$items = $menu["Horses"];

?>
<div id="main-container" class="mcc-horses mcc-nospace">




<div class="container-full">

	<div class="header-content">
		<div class="container-fluid">
			<div id="sub-navbar" class="navbar">
				<ul id="sub-nav" class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Horses <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
              	                <li><a href="index.php?p=horses_detail">Alberto II</a></li>
                                <li><a href="index.php?p=horses_detail">Chiara WZ</a></li>
                                <li><a href="index.php?p=horses_detail">For Fun</a></li>
                                <li><a href="index.php?p=horses_detail">Hero VD Roshoeve</a></li>
                                <li><a href="index.php?p=horses_detail">Wings Sublieme</a></li>
                                <li><a href="index.php?p=horses_detail">Zamiro 16</a></li>
						</ul>
					</li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div>


	<div class="content">

		<div class="main-content-container">
			<div class="container-fluid">


				<div class="horses">
			      <div class="feature-row row">
			      	<div class="col-md-12 fr-left">
								<ul class="main-slider slider">
								  <li><img src="assets/img/tmp/F14-409-199.jpg" /></li>
								  <li><img src="assets/img/tmp/F14-409-199.jpg" /></li>
								  <li><img src="assets/img/tmp/F14-409-199.jpg" /></li>
								</ul>			        	
			      	</div>
			      	
			        <div class="col-md-5 fr-right">
			        	<h3>Alberto II</h3>
								<ul class="content-list">
									<li class="clearfix">
										<div class="cl-left">Breed:</div>
										<div class="cl-right">Holsteiner</div>
									</li>

									<li class="clearfix">
										<div class="cl-left">Sex:</div>
										<div class="cl-right">Gelding</div>
									</li>

									<li class="clearfix">
										<div class="cl-left">DOB:</div>
										<div class="cl-right">2003</div>
									</li>

									<li class="clearfix">
										<div class="cl-left">Colour:</div>
										<div class="cl-right">Light Bay</div>
									</li>

									<li class="clearfix">
										<div class="cl-left">Sire:</div>
										<div class="cl-right">
											Acorado
										</div>
									</li>

									<li class="clearfix">
										<div class="cl-left">Dame:</div>
										<div class="cl-right">
											Hanka IV
										</div>
									</li>

									<li class="clearfix">
										<div class="cl-left">Height:</div>
										<div class="cl-right">
											16.3 HH
										</div>
									</li>

								</ul>
								
								<div class="details">
Alberto II – “Albert”, “Lulu”, “Baby Berto”<br/>
“I got him when I was young from Nick Skelton and over
the years we’ve built our careers together. He’s my horse
for many “firsts”… first Young Riders Team, first Nations
Cup and first Grand Prix. He’s a great horse… and he
knows it”
								</div><!-- details -->
								
			       	</div>
			      </div><!-- row -->

						<?php /*
			      <div class="horses-row row">
			        <div class="col-md-4 col-sm-6">
			        	<a href="#">
			        		<img src="assets/img/tmp/F14-284-195.jpg" />
			        		<h4>Chiara WZ</h4>
			        	</a>
			       	</div>
			        <div class="col-md-4 col-sm-6">
			        	<a href="#">
			        		<img src="assets/img/tmp/F14-284-195.jpg" />
			        		<h4>For Fun</h4>
			        	</a>
			       </div>
			        <div class="col-md-4 col-sm-6">
			        	<a href="#">
			        		<img src="assets/img/tmp/F14-284-195.jpg" />
			        		<h4>Zamiro 16</h4>
			        	</a>
			       </div>
			      </div><!-- row -->
			      */
			      ?>


		    </div><!-- horses -->



			</div>
		</div><!-- main-content-container -->

	</div><!-- content -->

</div><!-- container-full -->






</div><!-- main-container -->