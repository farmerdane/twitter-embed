<?php
global $menu;
$menu = array(

	"Horses" => array(
		"Alberto II" => true,
		"Chiara WZ" => true,
		"For Fun" => true,
		"Hero VD Roshoeve" => true,
		"Wings Sublieme" => true,
		"Zamiro 16" => true,
	),
	
	"News" => array(
		"Press" => true,
		"Videos" => true,
	),
	
	"About" => array(
		"Biography" => true,
		"Team" => true,
		"Results" => true,
		"StoneRidge" => true,
		"Gallery" => true,
	),
);

?><!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>

<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="assets/css/bootstrap.custom.min.css" rel="stylesheet" type="text/css" />
<link href="assets/css/styles.min.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lora:400' rel='stylesheet' type='text/css'>
<!--[if lt IE 8]>
	<link rel="stylesheet" href="css/ie.css">    
<![endif]-->

<script type="text/javascript" src="//yastatic.net/modernizr/2.7.1/modernizr.min.js"></script>
<script>window.Modernizr || document.write('<script src="assets/js/vendor/modernizr.min.js"><\/script>')</script>
	
</head>

<body>
    
    
    <div id="header" class="navbar navbar-default " role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">          	
            <span class="">Menu</span>
            <!-- img class="arrow" src="assets/img/bg_arrow_right.png" alt="" /-->
            <span class="arrow arrow-icon"></span>
          </button>
          <a class="navbar-brand" href="index.php">Kara Chad</a>
        </div>
        

        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          	<?php foreach($menu as $k => $v) :?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $k?> <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
              	<?php foreach($v as $k2 => $v2) :?>
                <li><a href="index.php?p=<?php if($k != "Horses"){ echo strtolower(str_replace(" ", "_",$k2)); }else{ echo 'horses'; } ?>"><?php echo $k2?></a></li>
                <?php endforeach; ?>
              </ul>
            </li>          	
          	<?php endforeach; ?>
          </ul>          
        </div><!--/.nav-collapse -->
        <a href="" id="logo"><img src="assets/img/logo_stoneridge.png" alt="Stoneridge - Kara Chad" /></a>
      </div>      
    </div>