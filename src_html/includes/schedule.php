<div id="main-container" class="mcc-general mcc-results">




<div class="container-full">		
	
	<div class="header-content">
		<div class="container">
			<h1>Schedule</h1>
		</div>
	</div>
	
	
	<div class="content">
		
		
		
		<div class="main-content-container mcc-team">
			
			<div class="container">
			      <div class="">
<h2>2014</h2>
	
<ul class="result-list schedule-list">
	<li>
		<h3>FTI Winter Equestrian Festival</h3>
		<p>
			January 8 - March 30</p><p>
			Wellington, FL	USA
		</p>
	</li>
	
	<li>
		<h3>CSIO Young Riders Nations Cup</h3>
		<p>
			February 26 - March 2</p><p>
			Wellington, FL	USA
		</p>
	</li>
	
	<li>
		<h3>Del Mar National Horse Show</h3>
		<p>
			April 29 - May 4</p><p>
			Del Mar, CA	USA
		</p>
	</li>	
  	
	<li>
		<h3>Furusiyya FEI Nations Cup CSIO 4* Xalapa</h3>
		<p>
			May 8 - 11 </p><p>
			Veracruz Mexico
		</p>
	</li>	
	
	<li>
		<h3>Spruce Meadows Summer Tournaments</h3>
		<p>
			June 4 - July 13</p><p>
			Calgary, AB	CANADA
		</p>
	</li>	

	<li>
		<h3>Sacramento International Horse Show</h3>
		<p>
			September 23 - October 5</p><p>
			Sacramento, CA	USA
		</p>
	</li>	
	
	<li>
		<h3>Del Mar International Welcome and World Cup</h3>
		<p>
			October 15 - 26</p><p>
			Del Mar, CA	USA
		</p>
	</li>	

	<li>
		<h3>Canadian National Team Nations Cup Tour</h3>
		<p>
			October - TBA</p><p>
			TBA	AGENTINA or BRAZIL 
		</p>
	</li>	
	
	<li>
		<h3>Los Angeles National Horse Show</h3>
		<p>
			November 5 - 9</p><p>
			Los Angeles, CA	USA
		</p>
	</li>	
	
	<li>
		<h3>Las Vegas National Horse Show</h3>
		<p>
			November 11 - 16</p><p>
			Las Vegas, NV	USA
		</p>
	</li>	
	
</ul>

	        </div>

    	</div>
			
		</div><!-- main-content-container -->
		
	</div><!-- content -->
	
</div><!-- container-full -->






</div><!-- main-container -->