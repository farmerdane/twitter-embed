<?php

global $menu;
$items = $menu["Horses"];

?>
<div id="main-container" class="mcc-horses mcc-nospace">




<div class="container-full">

	<div class="header-content">
		<div class="container-fluid">
			<div class="navbar-collapse">
				<ul id="sub-nav" class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Horses <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
              	                <li><a href="index.php?p=horses_detail">Alberto II</a></li>
                                <li><a href="index.php?p=horses_detail">Chiara WZ</a></li>
                                <li><a href="index.php?p=horses_detail">For Fun</a></li>
                                <li><a href="index.php?p=horses_detail">Hero VD Roshoeve</a></li>
                                <li><a href="index.php?p=horses_detail">Wings Sublieme</a></li>
                                <li><a href="index.php?p=horses_detail">Zamiro 16</a></li>
						</ul>
					</li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div>


	<div class="content">

		<div class="main-content-container">
			<div class="container-fluid">


				<div class="horses">
					<div class="row">
					
					        <div class="item col-md-4 col-sm-6 col-xs-12">
					        	<a href="index.php?p=horses_detail" class="clearfix">
					        		<img src="assets/img/tmp/F14-284-195.jpg" />
					        		<div class="item-right caption">
									  		<h5>Alberto II</h5>
					        		</div>
					        	</a>
					       	</div>
							
					        <div class="item col-md-4 col-sm-6 col-xs-12">
					        	<a href="index.php?p=horses_detail" class="clearfix">
					        		<img src="assets/img/tmp/F14-284-195.jpg" />
					        		<div class="item-right caption">
									  		<h5>Chiara WZ</h5>
					        		</div>
					        	</a>
					       	</div>
							
					        <div class="item col-md-4 col-sm-6 col-xs-12">
					        	<a href="index.php?p=horses_detail" class="clearfix">
					        		<img src="assets/img/tmp/F14-284-195.jpg" />
					        		<div class="item-right caption">
									  		<h5>For Fun</h5>
					        		</div>
					        	</a>
					       	</div>
							
					        <div class="item col-md-4 col-sm-6 col-xs-12">
					        	<a href="index.php?p=horses_detail" class="clearfix">
					        		<img src="assets/img/tmp/F14-284-195.jpg" />
					        		<div class="item-right caption">
									  		<h5>Hero VD Roshoeve</h5>
					        		</div>
					        	</a>
					       	</div>
							
					        <div class="item col-md-4 col-sm-6 col-xs-12">
					        	<a href="index.php?p=horses_detail" class="clearfix">
					        		<img src="assets/img/tmp/F14-284-195.jpg" />
					        		<div class="item-right caption">
									  		<h5>Wings Sublieme</h5>
					        		</div>
					        	</a>
					       	</div>
							
					        <div class="item col-md-4 col-sm-6 col-xs-12">
					        	<a href="index.php?p=horses_detail" class="clearfix">
					        		<img src="assets/img/tmp/F14-284-195.jpg" />
					        		<div class="item-right caption">
									  		<h5>Zamiro 16</h5>
					        		</div>
					        	</a>
					       	</div>
							
					</div>
				</div><!-- horses -->



			</div>
		</div><!-- main-content-container -->

	</div><!-- content -->

</div><!-- container-full -->






</div><!-- main-container -->