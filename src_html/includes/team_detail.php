<div id="main-container">




<div class="container-full">		
	
	<div class="header-content">
		<div class="container-fluid">
			<div id="sub-navbar" class="navbar">
				<ul id="sub-nav" class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Team <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="index.php?p=team_detail">Ben Maher</a></li>
							<li><a href="index.php?p=team_detail">Susie Schroer</a></li>
							<li><a href="index.php?p=team_detail">Richard Carvin Jr.</a></li>
							<li><a href="index.php?p=team_detail">Ashley Rohmer</a></li>
							<li><a href="index.php?p=team_detail">Robert and Laurel Chad</a></li>
						</ul>
					</li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div>
	
	
	<div class="content">
		
		
		
		<div class="main-content-container mcc-team">
			
			<div class="container">
	      <div class="row">
	        <div class="col-md-5">
	        	<div class="feature-caption-img-container">
					  	<img src="assets/img/tmp/BenBio9.jpg" />
					  	<div class="caption">
					  		<h5>Trainer</h5>
					  		<p><strong>Ben Maher</strong>, Oxfordshire, UK</p>
					  		<em>Training with the best – world #2 ranked Equestrian.</em>
					  	</div>	
	          </div><!-- feature-caption-img-container -->
	        </div>
	        <div class="col-md-7">	          
<h3>Ben Maher</h3>
<p>Ben Maher is currently the world #2 ranked show jumper.
  He represented Britain at the 2008 Beijing Olympics, 2009
  European Championships, Windsor, and 2012 London
  Olympics. He won the team jumping gold medal at the
  London Olympics with Team GB (their first team jumping
  gold medal for 60 years), won bronze at the 2011 Europeans
  Championships in Madrid, Spain and countless other
  titles at international tournaments throughout the world.</p>
<p>Ben began riding at the age of eight and after finishing
  school, he trained with Liz Edgar before travelling to Switzerland
  to further his education with Beat Mandli. He
  started making his mark on the international circuit in his
  early twenties and began building up a string of worldclass
  horses. By the age of 26, he had overtaken regular
  British team members to become ranked number one in
  the country and number five in the World, with consistent
  wins around the globe in some of the sport’s most prestigious
  competitions. He is also one of just two riders to
  have won both the Hickstead Derby and Speed Derby in
  the same year. With one of the strongest strings of horses
  he has ever had, Ben continues to produce the results that
  have made him a valuable part of Team GB both as an individual
  and a team participant.</p>
<p>Since August 2013, Ben has played an instrumental role in
  the growth and development of Kara’s equine career. As
  her trainer, he has focused on honing her skills and emphasizing
  the importance of horse health and the partnership
  with her horses. In a short period, his influence on Kara has
  resulted in her winning her first Grand Prix tournament at
  Vienna Masters Global Champions Tour and achieving numerous
  other personal best rides at tournaments in North
  America and Europe.</p>
  
			      <div class="footer-row row">
			      	<div class="container col-md-12">
				      	<div class="nav-row row">
					        <div class="nr-left col-xs-6">
									     	<a href="">&laquo; Coach - Susie Schroer</a>
									</div>
					        <div class="nr-right col-xs-6">
									     	<a href="">Coach - Richard Carvin Jr. &raquo;</a>
									</div>
								</div><!-- container -->
							</div><!-- footer-row -->
  
	       </div>
	      </div>
    	</div>
			
		</div><!-- main-content-container -->
		
	</div><!-- content -->
	
</div><!-- container-full -->






</div><!-- main-container -->