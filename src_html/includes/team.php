<div id="main-container">




<div class="container-full">		
	
	
	<div class="header-content">
		<div class="container-fluid">
			<div id="sub-navbar" class="navbar">
				<ul id="sub-nav" class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Team <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="index.php?p=team_detail">Ben Maher</a></li>
							<li><a href="index.php?p=team_detail">Susie Schroer</a></li>
							<li><a href="index.php?p=team_detail">Richard Carvin Jr.</a></li>
							<li><a href="index.php?p=team_detail">Ashley Rohmer</a></li>
							<li><a href="index.php?p=team_detail">Robert and Laurel Chad</a></li>
						</ul>
					</li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div>
	
	
	<div class="content">
		
		
		
		<div class="main-content-container mcc-team-main">
			
			<div class="container-fluid">
				
			      <div class="general-row general-row-even row">
			      	<div class="container">
				      	<!-- div class="gr-header clearfix">
				      		<h3>Bits</h3><a href="">View All</a>
				      	</div -->
				      	
				      	<div class="row">
				      		
					        <div class="item col-md-4 col-sm-6 col-xs-12">
					        	<a href="index.php?p=team_detail" class="clearfix">
					        		<img src="assets/img/tmp/F14-409-199.jpg" />
					        		<div class="item-right caption">
									  		<h5>Trainer</h5>
									  		<p><strong>Ben Maher</strong>, Oxfordshire, UK</p>
									  		<em class="hidden-xs">Training with the best – world #2 ranked Equestrian.</em>
					        		</div>
					        	</a>
					       	</div>

					        <div class="item col-md-4 col-sm-6 col-xs-12 caption-bg-xs">
					        	<a href="index.php?p=team_detail" class="clearfix">
					        		<img src="assets/img/tmp/F14-409-199.jpg" />
					        		<div class="item-right caption">
									  		<h5>Coach</h5>
									  		<p><strong>Susie Schroer</strong>, Los Angeles, CA</p>
									  		<em class="hidden-xs">A lifelong horsewoman and top trainer who is committed to helping her students reach their potential.</em>
					        		</div>
					        	</a>
					       	</div>
					       	
					        <div class="item col-md-4 col-sm-6 col-xs-12">
					        	<a href="index.php?p=team_detail" class="clearfix">
					        		<img src="assets/img/tmp/F14-409-199.jpg" />
					        		<div class="item-right caption">
									  		<h5>Coach</h5>
									  		<p><strong>Richard Carvin Jr.</strong>, Los Angeles, CA</p>
									  		<em class="hidden-xs">A veteran rider and trainer with a strong track record of success in the hunter and jumper rings.</em>
					        		</div>
					        	</a>
					       	</div>
					       	
					        <div class="item col-md-4 col-sm-6 col-xs-12 caption-bg-xs">
					        	<a href="index.php?p=team_detail" class="clearfix">
					        		<img src="assets/img/tmp/F14-409-199.jpg" />
					        		<div class="item-right caption">
									  		<h5>Groom</h5>
									  		<p><strong>Ashley Rohme</strong>, Calgary, AB</p>
									  		<em class="hidden-xs">Head Groom/Manager for Kara Chad</em>
					        		</div>
					        	</a>
					       	</div>
					       	
					        <div class="item col-md-4 col-sm-6 col-xs-12">
					        	<a href="index.php?p=team_detail" class="clearfix">
					        		<img src="assets/img/tmp/F14-409-199.jpg" />
					        		<div class="item-right caption">
									  		<h5>Management</h5>
									  		<p><strong>Robert and Laurel Chad</strong>, Owners</p>
									  		<em class="hidden-xs">An equestrian family committed to the sport of show jumping.</em>
					        		</div>
					        	</a>
					       	</div>		       	

			      		</div><!-- row -->				
				
				
    	</div><!-- container-fluid -->
			
		</div><!-- main-content-container -->
		
	</div><!-- content -->
	
</div><!-- container-full -->






</div><!-- main-container -->