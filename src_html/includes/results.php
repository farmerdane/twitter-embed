<div id="main-container" class="mcc-general mcc-results">




<div class="container-full">		
	
	<div class="header-content">
		<div class="container-fluid">
			<div id="sub-navbar" class="navbar">
				<ul id="sub-nav" class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Results <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="index.php?p=team_detail">2014</a></li>
							<li><a href="index.php?p=team_detail">2013</a></li>
							<li><a href="index.php?p=team_detail">2012</a></li>
							
						</ul>
					</li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div>
	
	
	<div class="content">
		
		
		
		<div class="main-content-container mcc-team">
			
			<div class="container">
			      <div class="">
<h2>2014</h2>
	
<ul class="result-list">
  <li>
    <h3>Wellington Equestrian Festival</h3>
    <h4>Wellington, FL</h4>
    <ul>
      <li>
        <h5>WEF 1 Mar-O-Lago Club “AA”</h5>
        <span class="date">January 8 – 12</span>
        <ul>
          <li><span>5th place – $30,000 Mar-a-Lago Club Grand Prix, <em>Zamiro 16</em></span></li>
        </ul>
      </li>
      <li>
        <h5>WEF 2 Wellington Equestrian Realty CSI 2* “AA”</h5>
        <span class="date">January 15 – 19</span>
        <ul>
          <li><span>6th place – $34,000 Ruby Et Violette WEF Challenge Round 2, <em>Alberto II</em></span></li>
          <li><span>4F – $50,000 Wellington Equestrian Realty Grand Prix CS, <em>Alberto II</em></span></li>
        </ul>
      </li>
      <li>
        <h5>WEF 3 Fidelity Investments CSI3* “AA”</h5>
        <span class="date">January 22 – 26</span>
        <ul>
          <li><span>4F – $125,000 Fidelity Investments Grand Prix CSI3*, <em>Alberto II</em></li></span></li>
        </ul>        
      <li>
        <h5>WEF 4 Ariat CSI2* “AA”</h5>
        <span class="date">January 29 – February 2</span>
        <ul>
          <li><span>7th place – $34,000 Suncast 1.50m Championship Jumper Classic, <em>Zamiro 16</em></span></li>
        </ul>
      </li>
    </ul>
  </li>
  
  
  <li>
    <h3>Wellington Equestrian Festival</h3>
    <h4>Wellington, FL</h4>
    <ul>
      <li>
        <h5>WEF 1 Mar-O-Lago Club “AA”</h5>
        <span class="date">January 8 – 12</span>
        <ul>
          <li><span>5th place – $30,000 Mar-a-Lago Club Grand Prix, <em>Zamiro 16</em></span></li>
        </ul>
      </li>
      <li>
        <h5>WEF 2 Wellington Equestrian Realty CSI 2* “AA”</h5>
        <span class="date">January 15 – 19</span>
        <ul>
          <li><span>6th place – $34,000 Ruby Et Violette WEF Challenge Round 2, <em>Alberto II</em></span></li>
          <li><span>4F – $50,000 Wellington Equestrian Realty Grand Prix CS, <em>Alberto II</em></span></li>
        </ul>
      </li>
      <li>
        <h5>WEF 3 Fidelity Investments CSI3* “AA”</h5>
        <span class="date">January 22 – 26</span>
        <ul>
          <li><span>4F – $125,000 Fidelity Investments Grand Prix CSI3*, <em>Alberto II</em></li></span></li>
        </ul>        
      <li>
        <h5>WEF 4 Ariat CSI2* “AA”</h5>
        <span class="date">January 29 – February 2</span>
        <ul>
          <li><span>7th place – $34,000 Suncast 1.50m Championship Jumper Classic, <em>Zamiro 16</em></span></li>
        </ul>
      </li>
    </ul>
  </li>  
  
</ul>

	        </div>

    	</div>
			
		</div><!-- main-content-container -->
		
	</div><!-- content -->
	
</div><!-- container-full -->






</div><!-- main-container -->