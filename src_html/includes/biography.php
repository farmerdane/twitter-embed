<div id="main-container">




<div class="container-full">		
	
	<div class="header-content">
		<div class="container">
			<h1>Biography</h1>
		</div>
	</div>
	
	
	<div class="content">
		<div class="main-img-container">
			<img src="assets/img/tmp/_Barn_Portrait_4291Crop2.jpg" />
		</div><!-- main-img-container -->
		
		<div class="main-content-container">
			<div class="container">

<ul class="content-list">
	<li class="clearfix">
		<div class="cl-left">Hometown:</div>
		<div class="cl-right">Calgary, Alberta, Canada</div>
	</li>
	
	<li class="clearfix">
		<div class="cl-left">Born:</div>
		<div class="cl-right">January 1996</div>
	</li>
	
	<li class="clearfix">
		<div class="cl-left">Trainers:</div>
		<div class="cl-right">Susie Schroer and Dick Carvin,
			Meadow Grove Farm, Los Angeles, CA<br/>
			Ben Maher, Oxfordshire, UK</div>
	</li>
	
	<li class="clearfix">
		<div class="cl-left">Current Horses:</div>
		<div class="cl-right">Alberto II
			Chiara WZ<br/>
			For Fun<br/>
			Hero<br/>
			Wings Sublieme<br/>
			Zamiro 16</div>
	</li>
	
	<li class="clearfix">
		<div class="cl-left">Equestrian Highlights:</div>
		<div class="cl-right">
			<a href="">2014</a><br/>
			<a href="">2013</a><br/>
			<a href="">2012</a><br/>		
		</div>
	</li>
</ul>



			</div>
		</div><!-- main-content-container -->
		
	</div><!-- content -->
	
</div><!-- container-full -->






</div><!-- main-container -->