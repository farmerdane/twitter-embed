<div id="main-container">




<div class="container-full">		
	
	<div class="header-content">
		<div class="container">
			<h1>StoneRidge</h1>
		</div>
	</div>
	
	
	<div class="content">
		<div class="main-img-container">
			<img src="assets/img/tmp/SS13020_sm.jpg" />
		</div><!-- main-img-container -->
		
		<div class="main-content-container">
			<div class="container">

		      <div class="row">
		        <div class="col-md-8">
							<p>Stone Ridge is home base for the Chad Family. Set in the
							spectacular foothills of Alberta’s Kananaskis Country only 30
							minutes from the world-renowned Equestrian center, Spruce
							Meadows. When in Calgary, Kara and Bretton Chad train at
							Stone Ridge, the family’s private equestrian facility.</p>
		        </div>
		        <div class="col-md-4">	       
		        	<img src="assets/img/tmp/IMG_1449.jpg" />
		       </div>
		      </div><!-- row -->

			</div>
		</div><!-- main-content-container -->
		
	</div><!-- content -->
	
</div><!-- container-full -->






</div><!-- main-container -->