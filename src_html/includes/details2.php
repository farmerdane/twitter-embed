<div id="main-container">




<div class="container-full">		
	
	<div class="header-content">
		<div class="container">
			<h1>Press</h1>
		</div>
	</div>
	
	
	<div class="content">
		
		
		
		<div class="main-content-container mcc-press">
			
			<div class="container">
	      <div class="row">
	        <div class="col-md-5">
	        	<div class="feature-caption-img-container">					  	
					  	<div class="caption">
			        	<div class="fr-header">
			        		<span class="fr-publication">Equine Canada Magazine</span> | <span class="fr-date">May 2014</span>
			        		<h2>Canada Leading North American League of 2014 Furusiyya FEI Nations Cup Series After Second Consecutive Win</h2>
			        	</div><!-- fr-header -->
					  	</div>	
					  	<img src="assets/img/tmp/BenBio9.jpg" />
	          </div><!-- feature-caption-img-container -->
	        </div>
	        <div class="col-md-7">	          
						<p>Team Canada continues to dominate the North American, Central
						  American and Caribbean League of the Furusiyya FEI Nations Cup,
						  after picking up their second consecutive win of the series on May
						  9 at the CSIO4* Coapexpan in Mexico.
						  </p>
						<p>Team members Kara Chad (Calgary, AB), Ian Millar (Perth, ON), Jonathon
						  Millar (Perth, ON), and Chris Sorensen (Caledon, ON) came
						  out on top of the CSIO4* $80,000 Furusiyya FEI Nations Cup after
						  edging out Team USA in an impressive jump-off between legendary
						  riders, Ian Millar and Beezie Madden of the USA.  </p>
						<p>Team Canada proved their strength right from the start in round
						  one of the competition, which saw only four clear rounds, with
						  two of them being laid down by Canadians. Sorensen was the trailblazer
						  of the team, tackling the challenging course set by Frederic
						  Cottier of France. Paired with Bobby, a 12-year-old Dutch Warmblood
						  gelding he co-owns with Britland Hughes, Sorensen had
						  just one rail and one time fault for a total of five faults. Jonathon
						  received an identical score aboard Millar Brooke Farm’s 10-year-old
						  Swedish Warmblood gelding, Calvin Klein.  </p>
						<p>Chad, who is just 18-years-old and was competing in her first-ever
						  senior Nations Cup, put in an impressive performance, achieving a
						  clear round for Canada aboard Stone Ridge Farms’ 11-year-old Holsteiner
						  gelding, Alberto II.  </p>
						<p>Ian added another clear round to Canada’s roster riding in the
						  anchor position aboard his 2012 Olympic partner, Star Power, a
						  13-year-old Dutch Warmblood gelding owned by Team Works.</p>
						  
			      <div class="footer-row row">
			      	<div class="container col-md-12">
				      	<div class="tag-row row">
					        <div class="tag-container">
									     	<span class="label">Tags:</span> <a href="">Bits</a>,  <a href="">Featured</a>
									</div>
								</div>
				      	<div class="nav-row row">
					        <div class="nr-left col-xs-4">
									     	<a href="">&laquo; Previous Article</a>
									</div>
					        <div class="nr-mid col-xs-4">
									     	<a href="">Back to List</a></a>
									</div>									
					        <div class="nr-right col-xs-4">
									     	<a href="">Next Article &raquo;</a>
									</div>
								</div><!-- container -->
							</div><!-- footer-row -->
						  
						  
	       </div>
	      </div>
    	</div>
			
		</div><!-- main-content-container -->
		
	</div><!-- content -->
	
</div><!-- container-full -->






</div><!-- main-container -->