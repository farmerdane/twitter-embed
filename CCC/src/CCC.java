import java.util.concurrent.*;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL; 
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class CCC {

	private static ExecutorService pool;
	private static final String alexa_base_url = "http://www.alexa.com/topsites/global;";
	private static final int alexa_num_of_pages = 4;

	public CCC(final int numberofThreads, List<String> urls) {
		pool = Executors.newFixedThreadPool(numberofThreads);

		for(String url : urls){
			pool.execute(new HTTPHeaderGrabber(url));
		}
	}

	public void startThreads() throws InterruptedException {
		pool.awaitTermination(20, TimeUnit.SECONDS);
	}

	public static List<String> grabSites(){
		Document doc;
		List<String> urlList = new ArrayList<String>();
		
		//create a sequential list of Alexa paginated pages to grab the URLs 
		for(int i = 0; i < alexa_num_of_pages; i++){
			try {
				//grab the Alexa page with Jsoup
				doc = Jsoup.connect(alexa_base_url+ new Integer(i).toString()).userAgent("Mozilla").timeout(5000).get();
				//using CSS selectors parse out the relevant parts of the document
				Elements els = doc.select(".site-listing .desc-paragraph a");
				
				//iterate through all the links that are parsed and add it to a list to return
				for(Element el : els){
					urlList.add((String) "http://"+el.text().toLowerCase());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}			
		}
		return urlList;
	}



	public static class HTTPHeaderGrabber implements Runnable {
		private final String url;

		HTTPHeaderGrabber(String url) {
			this.url = url;
		}

		public void run() {

			int code = 404;
			try {
				URL theURL = new URL(url);
				
				//get the HTTP header
				HttpURLConnection connection = (HttpURLConnection) theURL.openConnection();
				Map<String, List<String>> map = connection.getHeaderFields();
				code = connection.getResponseCode();
				
				// if it is a redirect make sure to grab the Location header				
				if(code == 301 ||code == 302){
					List<String> loc = null;
					String key = "";
					if(map.containsKey("Location")){
						key = "Location";
					}else if(map.containsKey("location")){
						key = "location";
					}

					// try to re-queue the URL if a redirect header is found
					if(!key.equals("")){
						loc = map.get(key);
						String header = loc.get(0);
						pool.execute(new HTTPHeaderGrabber(header));
					}
				}else{
					// if the HTTP response is valid look for the Cache-Control Directive
					List<String> cacheControl = map.get("Cache-Control");
					
					// if the Cache-Control directive does not exist
					if (cacheControl == null) {
						System.out.println(url + " - NO Cache-Control - Response Code: " + connection.getResponseCode());
					} else {
						// if the Cache-Control directive exists
						for (String header : cacheControl) {
							System.out.println(url + " - Cache-Control: " + header);
						}
					}
				}
			} catch (Exception e) {
				System.out.println("Uhhh. Make sure the URL is valid. Problem with: " + url);
			}
		}
	}


	public static void main(String[] args) throws InterruptedException {
		int numberofThreads = Runtime.getRuntime().availableProcessors();
		
		//Call a function to get the top 100 sites from Alexa
		List<String> urls = CCC.grabSites();

		CCC runner = new CCC(numberofThreads, urls);
		runner.startThreads();
	}
}