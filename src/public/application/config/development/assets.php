<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['asset_uri'] = "/assets/";
$config['asset_uri_img'] = $config['asset_uri']."images/";
$config['asset_uri_cache'] = $config['asset_uri']."cache/";
