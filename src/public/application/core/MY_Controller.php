<?php

class MY_Controller extends CI_Controller
{
    function __construct()
    {

        parent::__construct();

				setlocale(LC_ALL, 'en_US.UTF-8');

				$this->self_name = strtolower($this->router->class);
				$this->data['controller_name'] = $this->self_name;

				$this->setTitles();
				$serv = "http://".$_SERVER["HTTP_HOST"]."/";
				$this->data['base']	=	$serv;

				$this->uri_arr = $this->uri->uri_to_assoc();
				$this->currentslug = key($this->uri_arr);

				$this->data['assets_url'] = $this->config->item('asset_uri');
				$this->data['assets_url_img'] = $this->config->item('asset_uri_img');
				$this->data['assets_url_img_cdn'] = $this->config->item('assets_url_img_cdn');
    }



	protected function setPage($name){
		$this->data['page']	=	$name;
		$this->data['title'] = isset($this->pagetitles[$this->data['page']]) ? $this->pagetitles[$this->data['page']] : "";
	}

}