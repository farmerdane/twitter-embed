<?php

use OAuth\OAuth1\Service\Twitter;
use OAuth\Common\Storage\Session;
use OAuth\Common\Consumer\Credentials;


class TwitterModel extends CI_Model {
	

	function __construct() {
		session_start();		
		parent::__construct();				
	}
	
	public function isLoggedIn($redirect = true){
		// check if a user is logged in - if not redirect back home or return a boolean value
		if(isset($_SESSION['twitter_user'])){
			return true;			
		}else{
			if($redirect){
				header('Location: /');
			}
			return false;
		}
	}
	
	public function isValidOAuthToken(){
		// check if an OAuth Token is valid (i.e. the token returned is the same one saved in the session)
		$oauth_token = $this->input->get_post('oauth_token', TRUE);
		if($oauth_token && $_SESSION['oauth_token'] !== $oauth_token){
			session_destroy();
			$this->session->set_flashdata('message', array("type" =>"error","class" =>"danger", "text" => "Invlid Token - Please Log-in Again"));
			header('Location: /');
		}
	}
	
	public function getLoginUrl(){
		// get temporary credentials in order to get a valid login URL
		$connection = new TwitterOAuth($this->config->item('twitter_consumer_key'), $this->config->item('twitter_consumer_secret'));
		$cred = $connection->getRequestToken($this->config->item('twitter_callback_url'));
		
		// store the token and secret token in the session to re-use later and make subsequent calls to the API
		$_SESSION['oauth_token'] = $cred['oauth_token'];
		$_SESSION['oauth_token_secret'] = $cred['oauth_token_secret'];
		
		//get the login url and return it as a string
		$redirect_url = $connection->getAuthorizeURL($cred['oauth_token']);
		return $redirect_url;
	}
	
	// make the Twitter API call and save the credentials within a Session
	public function initUserTwitter($oauth_verifier){
		$temp_connection = new TwitterOAuth($this->config->item('twitter_consumer_key'), $this->config->item('twitter_consumer_secret'), $_SESSION['oauth_token'],$_SESSION['oauth_token_secret']);
		# get a long lasting token from Twitter to make API calls as the logged in user
		$creds = $temp_connection->getAccessToken($oauth_verifier);
				
	
		$connection = new TwitterOAuth($this->config->item('twitter_consumer_key'), $this->config->item('twitter_consumer_secret'), $creds['oauth_token'], $creds['oauth_token_secret']);
		$_SESSION['oauth_token'] = $cred['oauth_token'];
		$_SESSION['oauth_token_secret'] = $cred['oauth_token_secret'];
		
		# make an API call to grab the user credentials and store it in a session for app authentication purposes
		$account = $connection->get('account/verify_credentials');
		
		if($account){
			$_SESSION['twitter_user'] = array(
				"id" => $account->id_str,
				"name" => $account->name,
				"screen_name" => $account->screen_name,
				"profile_image_url_https" => $account->profile_image_url_https,
			);
			return true;
		}
		return false;
	}
	
	public function getTweets($user_id, $limit = 25){
		#make an API call to grab 25 tweets
		$connection = new TwitterOAuth($this->config->item('twitter_consumer_key'), $this->config->item('twitter_consumer_secret'), $_SESSION['oauth_token'],$_SESSION['oauth_token_secret']);		
		$tweets = $connection->get("https://api.twitter.com/1.1/statuses/user_timeline.json", array("user_id" => $user_id, "exclude_replies" => false, "include_rts " => false, "count" => $limit));
		return $tweets;
	}



}
