<?php $this->load->view('inc/header'); ?>
<body class="<?php echo $page ?>">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div id="logo" class="clearfix">
          	<a class="nav-logo">Farmer</a>
          	<a class="navbar-brand" href="#">Farmer's JS Tweet Embed Generator</a>
          </div>
        </div>
        <div id="navbar">
        	<?php if(isset($_SESSION['twitter_user'])) :  ?>
        		<a id="link-logout" class="btn btn-primary" href="/pages/logout">Logout</a>
        		<div id="user-container">
        			<img src="<?php echo $_SESSION['twitter_user']['profile_image_url_https']?>" alt="<?php echo $_SESSION['twitter_user']['name']?>" />
        			<p><?php echo $_SESSION['twitter_user']['name']?> (@<?php echo $_SESSION['twitter_user']['screen_name']?>)</p>
        		</div>        		
        	<?php else: ?>
          	<a id="link-signin" href="<?php echo $login_url ?>"><img src="https://g.twimg.com/dev/sites/default/files/images_documentation/sign-in-with-twitter-gray.png" alt="Sign-in with Twitter" /></a>
        	<?php endif; ?>
        </div><!--/.navbar -->
      </div>
    </nav>

    <div id="main-container" class="container-fluid">
    	
    	<?php 
    	$message = $this->session->flashdata('message');
    	if($message): ?>
    	<div class="alert alert-<?php echo $message['class'] ?>" role="alert"><?php echo $message['text'] ?></div>
    	<?php endif; ?>

			<?php
			if(is_file(APPPATH.'views/content/content_'. $page . EXT)){
				$this->load->view('content/content_'. $page);
			}
			?>

    </div><!-- /.container -->
	

	<?php $this->load->view('inc/footer'); ?>
</body>
</html>