<div class="content-container">
	<div class="cc-header">
		<h2 id="cch-feelmagic" class="ir">This festive season feel the magic - Claim your Magic Box, pass it on to your friends, and win air tickets, iPads, gift vouchers and loads of other exciting gifts.</h2>
	</div><!-- cc-header-->
	
	<div class="cc-body">
		<h3 style="margin: 0 auto; width: 80%; text-align: center;"><?php echo $message ?></h3>
	</div><!-- cc-body-->
	
	<?php if(isset($share) && $share) : ?>	
	<div class="cc-footer">
		<a href="<?php echo base_url() ?>pages/passiton/" id="passiton" class="redbutton rb-sendbox">Share the Magic Box with Friends</a>
	</div><!-- cc-footer-->
	<?php endif; ?>
		

	
	
</div><!-- content-container -->