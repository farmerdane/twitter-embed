<div id="container-tweet" class="content-container">
	<div class="inner-container">
		
		
		
		
		
		
<div class="tabpanel" role="tabpanel">
	
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#tweets" aria-controls="home" role="tab" data-toggle="tab">Your Tweets</a></li>
    <li role="presentation"><a href="#live_tweets" class="live-tweets" aria-controls="profile" role="tab" data-toggle="tab">Live Tweets</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="tweets">
			<div class="tweet-list">
				<ul>
					<?php foreach($tweets as $t): ?>
						<li data-tid="<?php echo $t->id_str ?>">
							<div class="media">
							  <a class="media-left" href="#">
							    <img src="<?php echo $t->user->profile_image_url_https ?>" alt="...">
							  </a>
							  <div class="media-body">
							    <h4 class="media-heading"><?php echo $t->user->name ?> (<?php echo $t->user->screen_name ?>)</h4>
							    <p>Tweet ID: <?php echo $t->id_str ?></p>
							    <span><?php echo $t->text ?></span>
							  </div>
							</div>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
    </div><!-- tab-panel -->
    <div role="tabpanel" class="tab-pane" id="live_tweets">
    	<a href="" class="btn btn-primary btn-xs" id="tweet-toggle">On</a>
    	<div id="tl-live" class="tweet-list">
    	</div>
    </div><!-- tab-panel -->
  </div>

</div>
		
		
		
		
		
		
		
		

	</div><!-- inner-conatiner -->


	
</div>

<div id="container-select" class="content-container">
	<div class="inner-container">
	<h2>Tweet Settings</h2>
<form class="form-horizontal" id="dataform">
	<input type="hidden" id="tid" name="tid" value="" />
  <fieldset>
    
    <div class="form-group">
      <label for="select" class="col-lg-12 control-label">Align</label>
      <div class="col-lg-12">
        <select class="form-control" id="select" name="align">
        	<option value="" selected>Select One</option>
          <option value="left">Left</option>
          <option value="center">Center</option>
          <option value="right">Right</option>
        </select>
      </div>
    </div>
    
    <div class="form-group">
      <label for="select" class="col-lg-12 control-label">Show Conversation</label>
      <div class="col-lg-12">
        <select class="form-control" id="select" name="conversation">
        	<option value="all" selected>All</option>
          <option value="none">None</option>
        </select>
      </div>
    </div>
    
    <div class="form-group">
      <label for="select" class="col-lg-12 control-label">Show Cards</label>
      <div class="col-lg-12">
        <select class="form-control" id="select" name="cards">
        	<option value="visible" selected>Visible</option>
          <option value="hidden">Hidden</option>
        </select>
      </div>
    </div>   
    
    <div class="form-group">
      <label for="inputEmail" class="col-lg-12 control-label">Width</label>
      <div class="col-lg-12">
				<div class="input-group">
						<input type="text" class="form-control" id="width" name="width" placeholder="e.g. 100">
						<span class="input-group-addon">px</span>
				</div>
      </div>      
    </div> 
    
    <div class="form-group">
      <label for="inputEmail" class="col-lg-12 control-label">Link Colour:</label>
      <div class="col-lg-12">
				<div class="input-group">
						<span class="input-group-addon">#</span>
						<input type="text" class="form-control" name="linkColor" id="linkColor" placeholder="e.g. ffffff">
				</div>
        
      </div>      
    </div>
    
    <div class="form-group">
      <label for="select" class="col-lg-12 control-label">Theme</label>
      <div class="col-lg-12">
        <select class="form-control" id="select" name="theme">
        	<option value="light" selected>Light</option>
          <option value="dark">Dark</option>
        </select>
      </div>
    </div>   
    
    <div class="form-group">
      <div class="col-lg-12">
      <br/>
        <button type="submit" class="btn btn-primary">Refresh</button>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
			  Get Code
				</button>

      </div>
    </div>
  </fieldset>
</form>		
	</div><!-- inner-conatiner -->
</div>

<div id="container-display" class="content-container">
	<div class="inner-container">
	<h2>Tweet Display</h2>
		<div id="tweet-item"><h6>Select a Tweet from the first pane</h6></div>
		
	</div><!-- inner-conatiner -->
</div>


<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title">Code Instructions</h4>
      </div>
      <div class="modal-body">
        <ol>
        	<li>
        		<p>Insert a div into your page</p>
        		<textarea class="sm"><div id="tweet-container"></div></textarea>
        	</li>
        	<li>
<p>Insert the twitter widget js script in your footer before the &lt;body&gt; tag:</p>
<textarea>
<script>window.twttr = (function (d, s, id) {
  var t, js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src= "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);
  return window.twttr || (t = { _e: [], ready: function (f) { t._e.push(f) } });
}(document, "script", "twitter-wjs"));</script>
</textarea>
        	</li>
					<li>
						<p>Insert the following js script to your page:</p>
					<textarea id="code-chunk-container">

					</textarea>
					</li>        	
        </ol>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<textarea id="code-chunk" class="hidden">
					<script>
					twttr.ready(function() {
						twttr.widgets.createTweet(
						'{id}',
						document.getElementById('tweet-container'),
							{options}
						)
						.then(function (el) {
						});
					});
					</script>
</textarea>