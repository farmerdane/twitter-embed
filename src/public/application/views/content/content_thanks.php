<div class="content-container">
	<div class="cc-header">
		<h2 id="cch-thanks" class="ir">Thanks</h2>
	</div><!-- cc-header-->
	
	<div class="cc-body">
		<div class="cc-body">
			<div id="ccb-present" class="present"></div>
		</div><!-- cc-body-->
	</div><!-- cc-body-->
	
	<div class="cc-footer">
		<a href="<?php echo base_url() ?>pages/passiton/" id="passiton" class="redbutton rb-passiton">Share the Magic Box with Friends</a>
	</div><!-- cc-footer-->
	
</div><!-- content-container -->