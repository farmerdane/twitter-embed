<div class="content-container">
	<?php if(isset($pid) && $pid !== false) : ?>
		<div class="cc-general">		
				<img id="cc-win" src="<?php echo $assets_url_img_cdn ?>win_<?php echo $pid ?>.png" alt="You won an <?php echo $pid ?>" />
				<img src="<?php echo $assets_url_img_cdn ?>win_footer.png" alt="Spread the cheer - Pass on the Magic Box to 10 of your friends and surprise them." />
		</div><!-- cc-header-->	
	<?php else: ?>
		
		<div class="cc-header">
			<h2 id="cch-closed" class="ir">Uh-oh Your Magic Box did not open.But wait, you can still win! Pass it on to 10 of your friends and if it opens for any of them, you both get the gift inside!</h2>
		</div><!-- cc-header-->	
		<div class="cc-body">
			<a id="ccb-present" class="present"></a>
		</div><!-- cc-body-->
		
	<?php endif; ?>
	
		
	<div class="cc-footer">
		<a href="<?php echo base_url() ?>pages/passiton/" id="passiton" class="redbutton rb-sendbox">Share the Magic Box with Friends</a>
	</div><!-- cc-footer-->
	
</div><!-- content-container -->

<?php if(isset($fb_sr['page']) && $fb_sr['page']['admin']) : ?>
	<!-- a href="https://www.facebook.com/add.php?api_key=<?php echo $this->config->item('fb_app_id') ?>&pages=1">Add App</a -->
<?php endif; ?>