<div class="content-container">
	<div class="cc-header">
		<h2 id="cch-spread" class="ir">Spread the cheer - Select friends to send them the Magic Box.</h2>
		<p><strong>When you send it to a friend, if they win, you also win the same prize! Share it with up to 10 friends below:</strong></p>
	</div><!-- cc-header-->	
	
	<div class="cc-body">
		<?php if(isset($friends['data']) && count($friends['data']) > 0) : ?>
			<div id="friendlist" class="clearfix">
			<?php foreach($friends['data'] as $f) : ?>	
				<div class="fl-cell">
					<div class="flc-inner">
						<img src="https://graph.facebook.com/<?php echo $f['id'] ?>/picture" alt="<?php echo htmlentities($f['name']) ?>" />
						<p><?php echo $f['name'] ?></p>						
					</div>
					<input type="checkbox" name="fl[]" value="<?php echo $f['id'] ?>" />
				</div><!-- fl-cell -->
			<?php endforeach; ?>	
			</div>
		
		<?php endif; ?>
	</div><!-- cc-body-->
		
	<div class="cc-footer">
		<a href="" id="sendbox" class="redbutton rb-sendbox">Share the Magic Box with Friends</a>
	</div><!-- cc-footer-->
	
</div><!-- content-container -->