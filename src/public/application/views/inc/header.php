<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<!–[if IE]><meta http-equiv="X-UA-Compatible"content="IE=9"><![endif]–>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<title><?php echo $title ?> - </title>
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">	
	<link type="text/css" rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootswatch/3.3.0/superhero/bootstrap.min.css" media="screen" />
	<link type="text/css" rel="stylesheet" href="<?php echo $assets_url ?>css/styles.min.css" media="screen" />
		
</head>