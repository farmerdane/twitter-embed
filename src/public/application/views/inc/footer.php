<!-- Footer -->
<div id="footer" class="cont-center">
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo $assets_url ?>js/jquery.min.js"><\/script>')</script>
<?php if($page == "dashboard"): ?>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="//js.pusher.com/2.2/pusher.min.js" type="text/javascript"></script>
<?php endif; ?>
<script>window.twttr = (function (d, s, id) {
  var t, js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src= "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);
  return window.twttr || (t = { _e: [], ready: function (f) { t._e.push(f) } });
}(document, "script", "twitter-wjs"));</script>
<script type='text/javascript' src='<?php echo $assets_url ?>js/vendor.min.js?ver=1.1'></script>
<script type='text/javascript' src='<?php echo $assets_url ?>js/scripts.min.js?ver=1.1'></script>	