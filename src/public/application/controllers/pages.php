<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MY_Controller {


	function __construct()
	{		
		parent::__construct();
	}

  protected function setTitles(){
  	$this->pagetitles["index"] = "Home";
  }
  
  public function logout(){
			session_destroy();
			header('Location: /');
  }
  
  //callback URL for Twitter Sign-in
  public function auth(){
			//grab the oauth_verifier parameter if it exists and log in the user
			$authed = $this->input->get_post('oauth_verifier', TRUE);
			if($authed){
				// make the Twitter API call and save the credentials within a Session
				$auth = $this->TwitterModel->initUserTwitter($authed);	
				
				if($auth){
					header('Location: /pages/dashboard');				
				}
			}else{
				header('Location: /');
			}
  }
  
  public function dashboard(){  
		// ensure the user is logged in
  		$this->TwitterModel->isLoggedIn();
			$this->setPage('dashboard');
			//get tweets for the logged in user
			$this->data['tweets'] = $this->TwitterModel->getTweets($_SESSION['twitter_user']["id"]);
			
			$this->load->view('container', $this->data);
	}
		
  public function index(){  
  		$this->TwitterModel->isValidOAuthToken();
  	 	
			$this->setPage('index');
			$this->data['login_url'] = "";
			
			if(isset($_SESSION['twitter_user'])){				
				header('Location: /pages/dashboard');
			}else{
				// if not logged in get a Login URL and send to the main page
				$this->data['login_url'] = $this->TwitterModel->getLoginUrl();
				$this->load->view('container', $this->data);
			}
			
			
	}
  
}
