<?php
require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/lib/config.php';
require_once __DIR__.'/lib/daemon.php';
require_once __DIR__.'/lib/twitterconsumer.php';

class MyDaemon extends DaemonPHP {

    public function run() {
			$sc = new TwitterConsumer(OAUTH_TOKEN, OAUTH_SECRET, Phirehose::METHOD_SAMPLE);
			$sc->consume();
    }
}

$daemon = new MyDaemon(__DIR__.'/tmp/twitterconsumer.pid');

$daemon->setLog(__DIR__.'/log/my.log')
        ->setErr(__DIR__.'/log/my.err')
        ->handle($argv);