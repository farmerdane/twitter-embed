<?php
class TwitterConsumer extends OauthPhirehose
{
	private $data_arr = array();
	private $pusher;
	
	function __construct($username, $password, $method = Phirehose::METHOD_SAMPLE, $format = self::FORMAT_JSON, $lang = FALSE){
		parent::__construct($username, $password, $method = Phirehose::METHOD_SAMPLE, $format = self::FORMAT_JSON, $lang = FALSE);
		$this->pusher = new Pusher( PUSHER_KEY, PUSHER_SECRET, PUSHER_APP_ID, false, 'https://api.pusherapp.com', 443 );
	}
	
	public function enqueueStatus($status)
	{
		$data = json_decode($status, true);
		if (is_array($data) && isset($data['user']['screen_name'])) {

				$data_item[] = array(
					"id" => $data['id_str'],
					"name" => $data['user']['name'],
					"screen_name" => $data['user']['screen_name'],
					"profile_image_url_https" => $data['user']['profile_image_url_https'],
					"created_at" => $data['created_at'],
					"text" => $data['text'],
				);
				
				if(count($this->data_arr) >= TWEET_NUM){
					var_dump("sent");
					$this->pusher->trigger( 'tweet-channel', 'tweet_event', $this->data_arr);
					$this->data_arr = array();
					sleep(TWEET_TIMEOUT);					
				}else{
					$this->data_arr[] = $data_item;
				}						
		}
		
		
	}
}